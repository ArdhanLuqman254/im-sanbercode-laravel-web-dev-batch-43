@extends('layouts.master')

@section('title')
Halaman Edit Data Cast
@endsection

@section('sub-title')
halaman edit data cast
@endsection

@section('content')

<a href="/cast" class="btn btn-primary bt-sm">Back</a>

<ul>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Cast Name</label>
            <input type="text" name="name" value="{{$cast->name}}" class="form-control" placeholder="Enter Cast Name">
        </div>
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Cast Age</label>
            <input type="number" name="age" value="{{$cast->age}}" class="form-control" placeholder="Enter Cast Age">
        </div>
        @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Cast Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10">{{$cast->description}}</textarea>
        </div>
        @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</ul>
@endsection