@extends('layouts.master')

@section('title')
Halaman Tambah Cast
@endsection

@section('sub-title')
halaman tambah cast
@endsection

@section('content')

<a href="/cast" class="btn btn-primary bt-sm">Back</a>

<ul>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Cast Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Cast Name">
        </div>
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Cast Age</label>
            <input type="number" name="age" class="form-control" placeholder="Enter Cast Age">
        </div>
        @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Cast Description</label>
            <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</ul>
@endsection