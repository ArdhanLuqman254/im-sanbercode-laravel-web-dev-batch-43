@extends('layouts.master')

@section('title')
Halaman Tampil Cast
@endsection

@section('sub-title')
halaman tampil cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary bt-sm">Add Cast</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->name}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <form action="/cast/{{$item->id}}" method="POST" class="d-inline">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
            </td>
        </tr>
        @empty
        <h1>Data Kosong</h1>
        @endforelse
    </tbody>
</table>

@endsection