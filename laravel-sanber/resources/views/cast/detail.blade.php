@extends('layouts.master')

@section('title')
Halaman Detail Cast
@endsection

@section('sub-title')
halaman detail cast
@endsection

@section('content')

<h1>{{$cast->name}}</h1>
<h2>{{$cast->age}} Years Old</h2>
<p>{{$cast->description}}</p>

@endsection