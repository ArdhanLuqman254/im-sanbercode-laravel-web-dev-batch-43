@extends('layouts.master')

@section('title')
    Halaman Register
@endsection

@section('sub-title')
    Register
@endsection

@section('content')
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="first"><br><br>
        <label>Last Name</label><br>
        <input type="text" name="last"> <br><br>
        <label>Gender:</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality:</label><br>
        <select name="Nation">
            <option value="Indo">Indonesia</option>
            <option value="US">Amerika</option>
            <option value="UK">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="lang">Indonesia<br>
        <input type="checkbox" name="lang">English<br>
        <input type="checkbox" name="lang">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="25"></textarea><br><br>
        <input type="submit" value="submit">
    </form>
@endsection