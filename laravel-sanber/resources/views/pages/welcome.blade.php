@extends('layouts.master')

@section('title')
    Halaman Welcome
@endsection

@section('sub-title')
    Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{$first_name}} {{$last_name}}!</h1>
    <h2>Terima Kasih telah bergabung di Sanberbook. Social Media kita bersama</h2>
@endsection