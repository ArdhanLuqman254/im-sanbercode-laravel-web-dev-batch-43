<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\authController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homeController::class, 'homes']);
Route::get('/register', [authController::class, 'pages']);

Route::post('/welcome', [authController::class, 'send']);

Route::get('/table', function () {
    return view('pages.table');
});

Route::get('/data-table', function () {
    return view('pages.data-table');
});

//CRUD Cast

//Create Data
//route untuk mengarahkan ke halaman form input cast
Route::get('/cast/create', [CastController::class, 'create']);

//route untuk menyimpan memasukkan inputan ke database
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//route untuk menampilkan data dari database
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);

//Update Data
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);