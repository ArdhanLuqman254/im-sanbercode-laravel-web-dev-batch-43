<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function pages()
    {
        return view('pages.register');
    }

    public function send(Request $request)
    {
        $first_name = $request['first'];
        $last_name = $request['last'];
        return view('pages.welcome', ['first_name' => $first_name, 'last_name' => $last_name]);
    }
}
